// +build go1.13

package pakparse

import (
	"errors"
	"fmt"
)

func fmtErrorf(format string, args ...interface{}) error {
	return fmt.Errorf(format, args...)
}

func errorAs(err error, target interface{}) bool {
	return errors.As(err, target)
}

func errorIs(err, target error) bool {
	return errors.Is(err, target)
}

func errorUnwrap(err error) error {
	return errors.Unwrap(err)
}
