/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// field_fixedval.go: Fixed-value field parsing

package pakparse

import (
	"reflect"
)

type fixedValueFieldParser struct {
	structFieldParser
	value []byte
}

var _ FieldParser = new(fixedValueFieldParser)

func (f *fixedValueFieldParser) Size() (int, bool) { return len(f.value), true }

func (f *fixedValueFieldParser) Consume(data []byte, state interface{}, packet reflect.Value) (int, interface{}, Status) {
	got, _ := state.(int)
	need := len(f.value) - got

	have := len(data)
	if have > need {
		have = need
	}

	for i, v := range data[:have] {
		if v == f.value[got] {
			got++
			continue
		}
		if got == 0 {
			return 1, 0, Invalid
		}
		return i, 0, Invalid
	}

	if got < len(f.value) {
		return have, got, Incomplete
	}

	f.unmarshalValue(f.value, packet)
	return have, nil, Complete
}

func (f *fixedValueFieldParser) Produce(packet reflect.Value) ([]byte, error) {
	return f.value, nil
}
