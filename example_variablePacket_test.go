/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// example_variablePacket_test.go: Complete example of parsing variable-length packets

package pakparse_test

import (
	"bytes"
	"fmt"
	"log"
	"reflect"

	"gitlab.com/rigel314/pakparse"
)

type ExampleVariablePacket struct {
	Magic    uint16 `pakparse:"value=0x1337"`
	Len      uint16
	Payload  []byte `pakparse:"len=Len,maxlen=260"`
	Checksum uint16
}

func Example_variablePacket() {
	c := new(pakparse.Config).Safe()

	// Add a validation function
	c.Check = func(x *ExampleVariablePacket) bool {
		// This function just returns true if Checksum is a hard coded value
		// A better validation would be to actually calculate the checksum and compare against the received Checksum
		return x.Checksum == 0x1234
	}

	// Assuming the protocol defines that Len includes the 2 Checksum bytes
	c.Count["Payload"] = func(x *ExampleVariablePacket) uint16 { return x.Len - 2 }

	p, err := pakparse.New(reflect.TypeOf(ExampleVariablePacket{}), c)
	if err != nil {
		log.Fatal(err)
	}

	// prepare some test data
	onesPl := [255]byte{}
	for i := 0; i < 255; i++ {
		onesPl[i] = 1
	}

	// send data into the parser
	s := new(pakparse.State)
	s.Consume(p, []byte{0x13, 0x37, 0x01, 0x01})
	s.Consume(p, onesPl[:])
	s.Consume(p, []byte{0x12, 0x34})

	x, ok := s.Next()
	assert(ok, "Expected a valid packet")

	v, ok := x.(ExampleVariablePacket)
	assert(ok, "Expected an ExampleVariablePacket")

	assert(v.Magic == 0x1337, "Expected Magic to equal 0x1337")
	assert(v.Len == 257, "Expected Len to equal 257")
	assert(bytes.Equal(v.Payload, onesPl[:]), "Expected Payload to equal [0, 1, ..., 254, 255]")
	assert(v.Checksum == 0x1234, "Expected checksum to equal 0x1234")

	fmt.Println("success")

	// Output:
	// success
}
