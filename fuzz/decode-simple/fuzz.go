// +build gofuzz

package pakparse

import (
	"bytes"
	"reflect"

	"gitlab.com/rigel314/pakparse"
)

func Fuzz(data []byte) int {
	p, err := pakparse.New(reflect.TypeOf(struct {
		Magic uint16 `pakparse:"value=0x1337"`
		A     float64
		B     float64
		C     float64
		D     float64
		Crc   uint16
	}{}), nil)
	if err != nil {
		panic(err)
	}

	s := new(pakparse.State)
	s.Consume(p, data)

	var n int
	for {
		k, ok := s.Next()
		if !ok {
			break
		}
		n++

		var buf bytes.Buffer
		err = s.Encode(p, &buf, k)
		if err != nil {
			panic(err)
		}
	}

	if n < 1 {
		return 0
	}
	return 1
}
