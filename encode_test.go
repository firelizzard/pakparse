/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// encode_test.go: Tests packet encoding

package pakparse_test

import (
	"bytes"
	"math"
	"testing"

	"gitlab.com/rigel314/pakparse"
)

func TestSimpleEncode(t *testing.T) {
	b := new(bytes.Buffer)
	p := mustCreateParser(t, SimplePacket{}, nil)

	new(pakparse.State).Encode(p, b, SimplePacket{
		A:   math.Pi,
		B:   math.Phi,
		C:   math.E,
		D:   math.Sqrt2,
		Crc: 0xDEAD,
	})

	requireBytes(t, b.Bytes(),
		encode{Big}.b2(0x1337),
		encode{Big}.f64(math.Pi),
		encode{Big}.f64(math.Phi),
		encode{Big}.f64(math.E),
		encode{Big}.f64(math.Sqrt2),
		encode{Big}.b2(0xDEAD),
	)
}

func TestSimpleEncodeCheckCalc(t *testing.T) {
	b := new(bytes.Buffer)
	p := mustCreateParser(t, SimplePacket{}, nil)

	t.Skip("feature SetCheckCalcFunction is not currently supported")
	// p.SetCheckCalcFunction(func(sp SimplePacket) SimplePacket {
	// 	sp.Crc = 0xDEAD
	// 	return sp
	// })

	new(pakparse.State).Encode(p, b, SimplePacket{
		A: math.Pi,
		B: math.Phi,
		C: math.E,
		D: math.Sqrt2,
	})

	requireBytes(t, b.Bytes(),
		encode{Big}.b2(0x1337),
		encode{Big}.f64(math.Pi),
		encode{Big}.f64(math.Phi),
		encode{Big}.f64(math.E),
		encode{Big}.f64(math.Sqrt2),
		encode{Big}.b2(0xDEAD),
	)
}
