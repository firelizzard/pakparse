/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// decode.go: Core packet parsing implementation

package pakparse

// Reset discards the current parsed packet state
func (s *State) Reset(p *Parser) {
	s.ensure(p, true)
}

// resets packet state
//
// if ok is false, increments discard counters
//
// if ok is true and the packet is complete, checks and enqueues the packet
func (s *State) reset(p *Parser, ok, field bool) {
	if !ok {
		s.packetsDiscarded++
		s.bytesDiscarded += len(s.packet)
		if s.Discard != nil {
			s.Discard(s.packet)
		}
	}

	if ok && s.field >= len(p.fields) {
		// end of packet, check and enqueue packet
		if p.check(s.outVal, s.packet) {
			s.packetsParsed++
			if s.Receive != nil {
				s.Receive(s.outVal)
			} else {
				s.outPackets.Push(s.outVal.Elem().Interface())
			}
		} else {
			s.packetsRejected++
			if s.Reject != nil {
				s.Reject(s.outVal)
			}
		}
	}

	s.field = 0

	// set length to 0, retain capacity
	if s.packet != nil {
		s.packet = s.packet[:0]
	}

	// reset the first field
	if field && len(p.fields) > 0 {
		s.fieldState[0] = p.fields[0].Reset(s.fieldState[0], s.outVal)
	}
}

func (s *State) reachedMaxInvalid() bool {
	if s.MaxInvalidBytes <= 0 {
		return len(s.packet) >= 512
	} else {
		return len(s.packet) >= s.MaxInvalidBytes
	}
}

// Step executes a parser step, processing the current field, returning the
// number of bytes read, any bytes that should be reprocessed, and the parsing
// status.
//
// If `n` is positive or zero, `unread` is nil. Otherwise, `n` is
// `-len(unread)`.
//
// If the current field parser returns a status of `Complete`, Step will
// increment the field counter. If the current field parser returns `Invalid`,
// the parser state will be reset, discarding the current packet. If a field
// parser returns `Incomplete`, Step will return.
func (s *State) Step(p *Parser, data []byte) (n int, unread []byte, status Status) {
	s.ensure(p, false)

	// get field, load state, process field, store state
	field := p.fields[s.field]
	state := s.fieldState[s.field]
	n, state, status = field.Consume(data, state, s.outVal)
	if status != Invalid && s.field == 0 && len(s.packet) > 0 {
		s.reset(p, false, false)
	}
	s.fieldState[s.field] = state

	s.bytesConsumed += n

	if n > 0 {
		if n > len(data) {
			panic(fmtErrorf("parser read more bytes than were provided for its consumption"))
		}

		// record consumed data
		s.packet = append(s.packet, data[:n]...)

	} else if n < 0 {
		j := len(s.packet) + n
		if j < 0 {
			panic(fmtErrorf("parser rewound more bytes than have been read"))
		}

		// rewind consumed data
		s.packet, unread = s.packet[:j], s.packet[j:]
	}

	switch status {
	case Invalid:
		// data is invalid, reset
		if s.field > 0 || s.reachedMaxInvalid() {
			s.reset(p, false, true)
		}

	case Incomplete:
		// field is incomplete, wait for more data
		if n < len(data) {
			panic(fmtErrorf("field is incomplete but data remains"))
		}

	case Complete:
		// next field
		s.field++

		// reset next field
		if s.field < len(p.fields) {
			s.fieldState[s.field] = p.fields[s.field].Reset(s.fieldState[s.field], s.outVal)
		}

	default:
		// field misbehaved and returned an invalid status
		panic(fmtErrorf("invalid status: %v", status))
	}

	return
}

// StepPacket calls Step until no more data is available or until a complete
// packet is parsed, returning the number of bytes read, any bytes that should
// be reprocessed, and the parsing status.
//
// If `n` is positive or zero, `unread` is nil. Otherwise, `n` is
// `-len(unread)`.
//
// StepPacket returns `Complete` when a complete packet has been parsed or
// `Incomplete` when more data is required. (When is `Invalid` returned?).
func (s *State) StepPacket(p *Parser, data []byte) (n int, unread []byte, status Status) {
	if len(data) == 0 {
		return 0, nil, Incomplete
	}

	s.ensure(p, false)

	var stall, pos, offset int
	for pos < len(data) && s.field < len(p.fields) {
		n, unread, status = s.Step(p, data[pos:])

		if n != 0 {
			stall = 0
		} else if stall >= 100 {
			panic(fmtErrorf("stalled"))
		} else {
			// no data consumed or rewound, increment stall counter
			stall++
		}

		pos += n
		offset += n
		if pos < 0 {
			// rewind past the beginning of `data`
			data = append(unread[:-pos], data...)
			pos = 0
		}
	}

	if offset < 0 {
		unread = data[:-offset]
	} else {
		unread = nil
	}

	if s.field < len(p.fields) {
		return offset, unread, Incomplete
	}

	if status != Complete {
		return offset, unread, status
	}

	s.reset(p, true, true)
	return offset, unread, status
}

// Consume calls StepPacket until no data remains.
func (s *State) Consume(p *Parser, data []byte) {
	// TODO: handle sync overrides current state
	// TODO: handle escape bytes

	var pos int
	for pos < len(data) {
		n, unread, status := s.StepPacket(p, data[pos:])

		pos += n
		if pos < 0 {
			// rewind past the beginning of `data`
			data = append(unread[:-pos], data...)
			pos = 0
		}

		if status != Complete && status != Invalid {
			return
		}
	}
}
