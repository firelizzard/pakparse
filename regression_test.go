/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// regression_test.go: Tests for regressions

package pakparse_test

import (
	"math"
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func TestIssue5(t *testing.T) {
	// https://gitlab.com/rigel314/pakparse/issues/5

	field := reflect.StructField{
		Index: []int{0},
		Type:  reflect.TypeOf([]byte{}),
	}

	config, err := pakparse.ParseFieldConfig(nil, field)
	require.NoError(t, err)

	config.Count = &pakparse.LengthConfig{Func: func(packet reflect.Value) int { return 10 }}
	f, err := pakparse.NewField(nil, field, config)
	require.NoError(t, err)

	n, state, status := f.Consume([]byte{1}, nil, reflect.Value{})

	require.Equal(t, pakparse.Incomplete, status)
	require.Equal(t, 1, n)
	require.NotNil(t, state)
}

func TestIssue7(t *testing.T) {
	// https://gitlab.com/rigel314/pakparse/issues/7

	b := []byte{}
	b = append(b, encode{Big}.b1(0x13)...)
	b = append(b, encode{Big}.b1(0x37)...)
	b = append(b, encode{Big}.f64(math.Pi)...)
	b = append(b, encode{Big}.f64(math.Phi)...)
	b = append(b, encode{Big}.f64(math.E)...)
	b = append(b, encode{Big}.f64(math.Sqrt2)...)
	b = append(b, encode{Big}.b2(0xDEAD)...)

	p := mustCreateParser(t, SimplePacket{}, &pakparse.Config{
		Check: func(data []byte) bool {
			require.Equal(t, b, data)
			return true
		},
	})

	s := new(pakparse.State)
	mustConsume(t, p, s, b[:1])
	mustConsume(t, p, s, b[1:])

	requirePacket(t, s, SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	})
}
