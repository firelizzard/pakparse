/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// coverage_test.go: Tests that exist solely for coverage

package pakparse_test

import (
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func TestCoverage(t *testing.T) {
	t.Run("Status", func(*testing.T) {
		_ = pakparse.Invalid.String()
		_ = pakparse.Incomplete.String()
		_ = pakparse.Complete.String()
		_ = pakparse.Status(-1).String()
	})

	t.Run("New", func(*testing.T) {
		type Packet struct{ V int32 }
		typ := reflect.TypeOf(Packet{})

		_, _ = pakparse.New(typ, new(pakparse.Config).SetCheck(1))
		_, _ = pakparse.New(typ, new(pakparse.Config).SetWidth("V", nil))
		_, _ = pakparse.New(typ, new(pakparse.Config).SetCount("V", nil))
		_, _ = pakparse.New(typ, new(pakparse.Config).SetParser("V", nil))
		_, _ = pakparse.New(reflect.TypeOf(struct{ V int }{}), nil)
	})

	t.Run("Decoder", func(t *testing.T) {
		var dec pakparse.Decoder
		require.Panics(t, func() { dec.BytesConsumed() })
		require.Panics(t, func() { dec.BytesDiscarded() })
		require.Panics(t, func() { dec.PacketsParsed() })
		require.Panics(t, func() { dec.ResetStats() })

		dec.SetPacketTimeout(time.Second)
		dec.SetReadTimeout(time.Second)
		dec.Close()
	})
}
