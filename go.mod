module gitlab.com/rigel314/pakparse

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dvyukov/go-fuzz v0.0.0-20210103155950-6a8e9d1f2415 // indirect
	github.com/elazarl/go-bindata-assetfs v1.0.1 // indirect
	github.com/stephens2424/writerset v1.0.2 // indirect
	github.com/stretchr/testify v1.6.1
	gitlab.com/go-utils/io v0.1.0
	golang.org/x/mod v0.4.1 // indirect
	golang.org/x/tools v0.1.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
