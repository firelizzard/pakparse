/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// fields_config.go: Field parser configuration

package pakparse

import (
	"encoding/binary"
	"math/big"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

// FieldConfig allows for tuning and customization of field parsing.
type FieldConfig struct {
	// Omit is used to omit fields from packet parsing.
	Omit bool

	// Order indicates the byte ordering to use when parsing a field.
	Order binary.ByteOrder

	// Parser is a custom function used to parse a field.
	Parser ParserFunction

	// Width is the byte-width of a field, or of members of a field for arrays.
	Width *LengthConfig

	// Count is the number of elements in an array field.
	Count *LengthConfig

	// MaxCount is the maximum element count for unbounded fields.
	MaxCount int

	// Value is the value of fixed-value fields.
	Value *big.Int
}

// LengthConfig is used to specify the width or count of a field.
type LengthConfig struct {
	// Fixed indicates whether the width or count of the field is fixed
	Fixed bool

	// Value is the fixed width or count of a field
	Value int

	// Func calculates the width or count of a field
	Func LengthFunction
}

// Calculate returns the calculated length of a field, given the current packet.
//
// If Fixed is true, Calculate returns Value. If `c` is nil, Calculate returns
// -1. If Fixed is false and Func is nil, Calculate returns -1. If `packet` is
// invalid, Calculate returns -1. Otherwise, Calculate calls Func and returns
// the result.
func (c *LengthConfig) Calculate(packet reflect.Value) int {
	if c == nil {
		return -1
	}

	if c.Fixed {
		return c.Value
	}

	if c.Func == nil {
		return -1
	}

	if !packet.IsValid() {
		return -1
	}

	return c.Func(packet)
}

// IsFixed returns `c.Fixed`, or false if `c` is nil.
func (c *LengthConfig) IsFixed() bool {
	return c != nil && c.Fixed
}

// CanCalculate returns whether the length can be calculated
func (c *LengthConfig) CanCalculate() bool {
	return c != nil && (c.Fixed || c.Func != nil)
}

var reTagSplit = regexp.MustCompile("[, ]+")

// ParseFieldConfig parses field parser configuration, given the packet type,
// field type, and field tag.
func ParseFieldConfig(packetType reflect.Type, field reflect.StructField) (*FieldConfig, error) {
	tag := field.Tag.Get("pakparse")
	if tag == "-" {
		return &FieldConfig{Omit: true}, nil
	}

	c := &FieldConfig{}
	canEndian, typeSize, err := verifyFieldKind(field.Type)
	if err != nil {
		return nil, err
	}

	if typeSize > 0 {
		c.SetWidth(typeSize)
	}

	if field.Type.Kind() == reflect.Array {
		c.SetCount(field.Type.Len())
	}

	if tag == "" {
		return c, nil
	}

	for _, arg := range reTagSplit.Split(tag, -1) {
		args := strings.SplitN(arg, "=", 2)
		key, value := args[0], ""
		if len(args) == 2 {
			value = args[1]
		}

		err := c.parseTagArgument(packetType, field, canEndian, typeSize, key, key, value)
		if err != nil {
			return nil, fmtErrorf("invalid tag argument %q: %w", arg, err)
		}
	}

	return c, nil
}

func verifyFieldKind(typ reflect.Type) (canEndian bool, typeSize int, err error) {
	switch kind := typ.Kind(); kind {
	default:
		return false, 0, fmtErrorf("field type is not valid")
	case reflect.Int, reflect.Uint:
		return false, 0, fmtErrorf("integer types with machine-specific widths are unsupported")
	case reflect.Chan, reflect.Func, reflect.Interface, reflect.Map, reflect.UnsafePointer, reflect.Uintptr:
		return false, 0, fmtErrorf("fields of type kind %s are unsupported", kind)

	case reflect.Bool, reflect.Int8, reflect.Uint8:
		typeSize = int(typ.Size())

	case reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64,
		reflect.Complex64, reflect.Complex128:
		canEndian = true
		typeSize = int(typ.Size())

	case reflect.Array, reflect.Slice:
		elem := typ.Elem()
		if elem.Kind() == reflect.Array || elem.Kind() == reflect.Slice {
			return false, 0, fmtErrorf("nested arrays and slices are unsupported")
		}
		canEndian, typeSize, err = verifyFieldKind(elem)
		// if typeSize > 0 && kind == reflect.Array {
		// 	typeSize *= typ.Len()
		// }

	case reflect.String, reflect.Struct:

	case reflect.Ptr:
		if typ.Elem().Kind() != reflect.Struct {
			return false, 0, fmtErrorf("fields of type %s are unsupported", typ)
		}
	}

	return canEndian, typeSize, err
}

func (c *FieldConfig) parseTagArgument(packetType reflect.Type, field reflect.StructField, canEndian bool, typeSize int, as, key, value string) error {
	var err error
	switch as {
	case "le", "little-endian":
		c.Order = binary.LittleEndian
		if !canEndian {
			return fmtErrorf("endianness is not valid for type %v", field.Type)
		}

	case "be", "big-endian":
		c.Order = binary.BigEndian
		if !canEndian {
			return fmtErrorf("endianness is not valid for type %v", field.Type)
		}

	case "endian", "order":
		switch value {
		case "le", "little":
			c.Order = binary.LittleEndian
		case "be", "big":
			c.Order = binary.BigEndian
		default:
			return fmtErrorf("invalid endianness %q", value)
		}
		if !canEndian {
			return fmtErrorf("endianness is not valid for type %v", field.Type)
		}

	case "data", "value":
		c.Value = new(big.Int)
		if value == "" {
			return fmtErrorf("tag argument %q must have a value", key)
		}
		err = c.Value.UnmarshalText([]byte(value))
		if err != nil {
			return fmtErrorf("failed to convert %q to a value", value)
		}

	case "len", "vlen":
		switch field.Type.Kind() {
		case reflect.Array:
			return fmtErrorf("use `width` for arrays instead of %q", key)

		case reflect.Slice:
			if elKind := field.Type.Elem().Kind(); elKind != reflect.Uint8 && elKind != reflect.Int8 {
				return fmtErrorf("use `width` or `count`, %q is only valid for []byte, []uint8, and []int8", key)
			}
			return c.parseTagArgument(packetType, field, canEndian, typeSize, "count", key, value)
		}

		return c.parseTagArgument(packetType, field, canEndian, typeSize, "width", key, value)

	case "count":
		if field.Type.Kind() != reflect.Slice {
			return fmtErrorf("%q is only valid for slices", key)
		}

		c.Count, err = parseLengthConfig(packetType, key, value)
		if err != nil {
			return err
		}

	case "width":
		c.Width, err = parseLengthConfig(packetType, key, value)
		if err != nil {
			return err
		}
		if c.Width.Fixed && c.Width.Value > typeSize {
			return fmtErrorf("field width %d is larger than sizeof(%v)", c.Width.Value, field.Type)
		}

	case "maxlen", "maxlength", "maxcount":
		c.MaxCount, err = strconv.Atoi(value)
		if err != nil {
			return fmtErrorf("%w", err)
		}
		if c.MaxCount < 1 {
			return fmtErrorf("%d is invalid", c.MaxCount)
		}

	default:
		return fmtErrorf("invalid key %q", key)
	}

	if canEndian && c.Order == nil {
		c.Order = binary.BigEndian
	}

	return nil
}

func parseLengthConfig(typ reflect.Type, key, value string) (*LengthConfig, error) {
	v, err := strconv.Atoi(value)
	if err == nil {
		return &LengthConfig{Fixed: true, Value: v}, nil
	}

	if typ == nil {
		return nil, fmtErrorf("%q is not a number and no packet type was specified", value)
	}

	field, ok := typ.FieldByName(value)
	if !ok {
		return nil, fmtErrorf("%q is not a number or the name of a field", value)
	}

	signed, unsigned := isIntType(field.Type)
	if signed {
		return &LengthConfig{Func: func(v reflect.Value) int { return int(v.Elem().Field(field.Index[0]).Int()) }}, nil
	} else if unsigned {
		return &LengthConfig{Func: func(v reflect.Value) int { return int(v.Elem().Field(field.Index[0]).Uint()) }}, nil
	} else {
		return nil, fmtErrorf("field %s is not an integer", value)
	}
}

// SetParser sets the parser function to use for the field.
//
// See VerifyParserFunction.
func (c *FieldConfig) SetParser(packetType reflect.Type, fn interface{}) error {
	vfn, err := VerifyParserFunction(packetType, fn)
	if err != nil {
		return err
	}

	c.Parser = vfn
	return nil
}

// SetWidth sets the width of the field.
//
// See VerifyLengthFunction.
func (c *FieldConfig) SetWidth(v int) {
	c.Width = &LengthConfig{Fixed: true, Value: v}
}

// SetWidthFunc sets the width function to use for the field.
//
// See VerifyLengthFunction.
func (c *FieldConfig) SetWidthFunc(packetType reflect.Type, fn interface{}) error {
	vfn, err := VerifyLengthFunction(packetType, fn)
	if err != nil {
		return err
	}

	c.Width = &LengthConfig{Func: vfn}
	return nil
}

// SetCount sets the count of the field.
//
// See VerifyLengthFunction.
func (c *FieldConfig) SetCount(v int) {
	c.Count = &LengthConfig{Fixed: true, Value: v}
}

// SetCountFunc sets the count function to use for the field.
//
// See VerifyLengthFunction.
func (c *FieldConfig) SetCountFunc(packetType reflect.Type, fn interface{}) error {
	vfn, err := VerifyLengthFunction(packetType, fn)
	if err != nil {
		return err
	}

	c.Count = &LengthConfig{Func: vfn}
	return nil
}

// SetIntValue sets the field value to an int.
func (c *FieldConfig) SetIntValue(v int64) {
	c.Value = big.NewInt(v)
}

// SetByteValue sets the field value to a byte sequence.
func (c *FieldConfig) SetByteValue(v ...byte) {
	c.Value = new(big.Int).SetBytes(v)
}
