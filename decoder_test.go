/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// decoder_test.go: Tests packet decoder

package pakparse_test

import (
	"io"
	"math"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func TestSimpleTimeoutDecode(t *testing.T) {
	w := make(chan []byte)
	r := make(chan SimplePacket)

	p := mustCreateParser(t, SimplePacket{}, nil)
	d := pakparse.NewDecoderWithChannel(p, w)
	d.SetPacketTimeout(100 * time.Millisecond)

	go func() {
		w <- []byte{0x13, 0x37}
		w <- encode{Big}.f64(math.Pi)
		w <- encode{Big}.f64(math.Phi)
		w <- encode{Big}.f64(math.E)
		w <- encode{Big}.f64(math.Sqrt2)
		w <- []byte{0xDE}
		time.Sleep(150 * time.Millisecond)
		w <- []byte{0xAD}
		w <- []byte{0x13, 0x37}
		w <- encode{Big}.f64(math.Phi)
		w <- encode{Big}.f64(math.Pi)
		w <- encode{Big}.f64(math.E)
		w <- encode{Big}.f64(math.Sqrt2)
		w <- []byte{0xDE, 0xAD}

		time.Sleep(10 * time.Second)
		close(w)
	}()

	go func() {
		defer close(r)

		var v SimplePacket
		var err error
		for {
			requireNotPanics(t, func() {
				err = d.Decode(&v)
			})
			if err == io.EOF {
				return
			}
			r <- v
		}
	}()

	select {
	case x := <-r:
		require.Equal(t, SimplePacket{
			Magic: 0x1337,
			A:     math.Phi,
			B:     math.Pi,
			C:     math.E,
			D:     math.Sqrt2,
			Crc:   0xDEAD,
		}, x)

	case <-time.After(time.Second):
		t.Fatal("timeout waiting for packet")
	}

	select {
	case <-r:
		t.Fatal("got two packets")

	case <-time.After(time.Second):
	}
}
