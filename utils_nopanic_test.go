// +build !panic

package pakparse_test

import (
	"os"
	"path/filepath"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

const dumpTestData = false

var testData = map[string][]byte{}
var testMu sync.Mutex

func mustConsume(t require.TestingT, p *pakparse.Parser, s *pakparse.State, data []byte) {
	helper(t).Helper()

	if t, ok := t.(*testing.T); dumpTestData && ok {
		testMu.Lock()
		if b, ok := testData[t.Name()]; ok {
			testData[t.Name()] = append(b, data...)
		} else {
			testData[t.Name()] = append([]byte{}, data...)
			t.Cleanup(func() {
				f, err := os.Create(filepath.Join("corpus", t.Name()))
				if err != nil {
					t.Fatal(err)
				}
				f.Write(testData[t.Name()])
			})
		}
		testMu.Unlock()
	}

	requireNotPanics(t, func() {
		helper(t).Helper()

		s.Consume(p, data)
	})
}
