/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// field_nested.go: Slice/array-of-struct field parsing

package pakparse

import (
	"reflect"
	"strings"
)

type nestedArrayParser struct {
	structFieldParser
	parser *Parser
}

type nestedArrayState struct {
	i   int
	s   *State
	got []byte
}

func (n *nestedArrayParser) Size() (int, bool) { return -1, false }

func (n *nestedArrayParser) Reset(state interface{}, packet reflect.Value) interface{} {
	f := packet.Elem().Field(n.idx)
	s := &nestedArrayState{s: new(State)}
	if f.Kind() == reflect.Array {
		return s
	}

	c := n.config.Count.Calculate(packet)
	if c <= 0 {
		return s
	}

	f.Set(reflect.MakeSlice(f.Type(), c, c))
	return s
}

func (n *nestedArrayParser) Produce(packet reflect.Value) ([]byte, error) {
	f := packet.Elem().Field(n.idx)
	l := f.Len()
	b := new(strings.Builder)
	s := new(State)

	for i := 0; i < l; i++ {
		v, err := s.Produce(n.parser, f.Index(i))
		if err != nil {
			return nil, err
		}
		b.Write(v)
	}

	return []byte(b.String()), nil
}

type nestedArrayParserWithCount struct {
	nestedArrayParser
}

func (n *nestedArrayParserWithCount) Consume(data []byte, state interface{}, packet reflect.Value) (int, interface{}, Status) {
	f := packet.Elem().Field(n.idx)
	s := state.(*nestedArrayState)

	var N int
	for N < len(data) {
		if s.i >= f.Len() {
			return 0, nil, Complete
		}

		n, _, status := s.s.StepPacket(n.parser, data[N:])
		if n < 0 {
			panic(fmtErrorf("nested packet parser returned negative bytes read"))
		}

		N += n
		if status == Complete {
			x, ok := s.s.Next()
			if !ok {
				return N, s, Invalid
			}
			f.Index(s.i).Set(reflect.ValueOf(x))
			s.i++
		} else {
			return N, s, status
		}
	}

	return N, s, Incomplete
}

type nestedArrayParserWithStop struct {
	nestedArrayParser
	intermediateWidth int
	stopSequence      []byte
}

func (n *nestedArrayParserWithStop) SetStop(intermediate int, sequence []byte) {
	n.intermediateWidth = intermediate
	n.stopSequence = sequence
}

func (n *nestedArrayParserWithStop) Consume(data []byte, state interface{}, packet reflect.Value) (int, interface{}, Status) {
	panic("nope")
}
