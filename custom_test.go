/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// custom_test.go: Tests custom parser callbacks

package pakparse_test

import (
	"math"
	"reflect"
	"testing"

	"gitlab.com/rigel314/pakparse"
)

func TestSimpleCustomDecode(t *testing.T) {
	type Packet struct {
		Magic uint16 `pakparse:"value=0x1337"`
		A     float64
		B     float64
		Crc   uint16
	}

	p := mustCreateParser(t, Packet{}, &pakparse.Config{
		Parser: map[string]interface{}{
			"B": func(reset bool, data []byte, _ interface{}, x *Packet) (int, interface{}, pakparse.Status) {
				if reset {
					return 0, nil, pakparse.Invalid
				}

				x.B = math.Sqrt(x.A)
				return 0, nil, pakparse.Complete
			},
		},
	})

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b2(0x1337))
	mustConsume(t, p, s, encode{Big}.f64(64))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

	requirePacket(t, s, Packet{
		Magic: 0x1337,
		A:     64,
		B:     8,
		Crc:   0xDEAD,
	})
}

func TestNestedDecode(t *testing.T) {
	type TestFunc func(t *testing.T) *pakparse.Parser

	type InnerPacket struct {
		B float64
		C float64
		D float64
	}

	t.Run("Simple", func(t *testing.T) {
		type Packet struct {
			Magic uint16 `pakparse:"value=0x1337"`
			A     float64
			Inner InnerPacket
			Crc   uint16
		}

		var tests = map[string]TestFunc{
			"Implicit": func(t *testing.T) *pakparse.Parser {
				return mustCreateParser(t, Packet{}, nil)
			},
			"Parser": func(t *testing.T) *pakparse.Parser {
				ip := mustCreateParser(t, InnerPacket{}, nil)

				return mustCreateParser(t, Packet{}, &pakparse.Config{
					Parser: map[string]interface{}{"Inner": ip},
				})
			},
			"Custom": func(t *testing.T) *pakparse.Parser {
				ip := mustCreateParser(t, InnerPacket{}, nil)

				return mustCreateParser(t, Packet{}, &pakparse.Config{
					Parser: map[string]interface{}{
						"Inner": func(reset bool, data []byte, s *pakparse.State, x *Packet) (int, *pakparse.State, pakparse.Status) {
							if s == nil {
								s = new(pakparse.State)
							}

							if reset {
								s.Reset(ip)
								return 0, s, pakparse.Invalid
							}

							var n int
							var v, ok = s.Next()
							for n < len(data) && !ok {
								s.Consume(ip, data[n:n+1])
								v, ok = s.Next()
								n++
							}

							if v != nil {
								reflect.ValueOf(&x.Inner).Elem().Set(reflect.ValueOf(v))
								return n, s, pakparse.Complete
							}
							return n, s, pakparse.Incomplete
						},
					},
				})
			},
		}

		for name, test := range tests {
			t.Run(name, func(t *testing.T) {
				p := test(t)

				s := new(pakparse.State)
				mustConsume(t, p, s, encode{Big}.b2(0x1337))
				mustConsume(t, p, s, encode{Big}.f64(math.Pi))
				mustConsume(t, p, s, encode{Big}.f64(math.Phi))
				mustConsume(t, p, s, encode{Big}.f64(math.E))
				mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
				mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

				requirePacket(t, s, Packet{
					Magic: 0x1337,
					A:     math.Pi,
					Inner: InnerPacket{
						B: math.Phi,
						C: math.E,
						D: math.Sqrt2,
					},
					Crc: 0xDEAD,
				})
			})
		}
	})

	t.Run("Slice", func(t *testing.T) {
		type Packet struct {
			Magic uint16 `pakparse:"value=0x1337"`
			A     float64
			Inner []InnerPacket
			Stop  uint16 `pakparse:"value=0x1337"`
			Crc   uint16
		}

		var tests = map[string]TestFunc{
			"Unbound": func(t *testing.T) *pakparse.Parser {
				t.Skip("parsing nested slice-of-struct fields is not implemented")
				return mustCreateParser(t, Packet{}, nil)
			},
			"Bound": func(t *testing.T) *pakparse.Parser {
				return mustCreateParser(t, Packet{}, &pakparse.Config{
					Count: map[string]interface{}{
						"Inner": func(*Packet) int { return 2 },
					},
				})
			},
		}

		for name, test := range tests {
			t.Run(name, func(t *testing.T) {
				p := test(t)

				s := new(pakparse.State)
				mustConsume(t, p, s, encode{Big}.b2(0x1337))
				mustConsume(t, p, s, encode{Big}.f64(math.Pi))
				mustConsume(t, p, s, encode{Big}.f64(math.Phi))
				mustConsume(t, p, s, encode{Big}.f64(math.E))
				mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
				mustConsume(t, p, s, encode{Big}.f64(2*math.Phi))
				mustConsume(t, p, s, encode{Big}.f64(2*math.E))
				mustConsume(t, p, s, encode{Big}.f64(2*math.Sqrt2))
				mustConsume(t, p, s, encode{Big}.b2(0x1337))
				mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

				requirePacket(t, s, Packet{
					Magic: 0x1337,
					A:     math.Pi,
					Inner: []InnerPacket{
						{
							B: math.Phi,
							C: math.E,
							D: math.Sqrt2,
						},
						{
							B: 2 * math.Phi,
							C: 2 * math.E,
							D: 2 * math.Sqrt2,
						},
					},
					Stop: 0x1337,
					Crc:  0xDEAD,
				})
			})
		}
	})
}
