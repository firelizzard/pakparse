/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// decoder_channel.go: Channel-based decoder implementation

package pakparse

import (
	"time"
)

type channelDecoder struct {
	source  <-chan []byte
	timeout time.Duration
}

func (d *channelDecoder) decode(p *Parser, s *State) error {
	// create a timeout channel
	var timeout <-chan time.Time
	if d.timeout != 0 {
		timeout = time.After(d.timeout)
	}

	select {
	case <-timeout:
		// reset the parser
		s.Reset(p)

	case data, ok := <-d.source:
		if !ok {
			return ErrClosed
		}

		// process data
		s.Consume(p, data)
	}

	return nil
}

func (d *channelDecoder) setTimeout(t time.Duration) {
	d.timeout = t
}
