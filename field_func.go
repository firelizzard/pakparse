/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// field_func.go: Callback-based field parsing

package pakparse

import (
	"reflect"
)

type funcFieldParser struct {
	structFieldParser
	parse   ParserFunction
	produce Marshaller
}

var _ FieldParser = new(funcFieldParser)

func (c *funcFieldParser) Size() (int, bool) { return -1, false }

func (c *funcFieldParser) getState(state interface{}) reflect.Value {
	if state == nil {
		return reflect.Value{}
	}
	return state.(reflect.Value)
}

func (c *funcFieldParser) Reset(state interface{}, packet reflect.Value) interface{} {
	_, state, _ = c.parse(true, nil, c.getState(state), packet)
	return state
}

func (c *funcFieldParser) Consume(data []byte, state interface{}, packet reflect.Value) (int, interface{}, Status) {
	if c.parse == nil {
		panic(fmtErrorf("cannot consume: no parser"))
	}
	return c.parse(false, data, c.getState(state), packet)
}

func (c *funcFieldParser) Produce(packet reflect.Value) ([]byte, error) {
	if c.produce == nil {
		return nil, fmtErrorf("cannot produce: no marshaller")
	}
	return c.produce(packet), nil
}
