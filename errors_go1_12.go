// +build !go1.13

package pakparse

import (
	"golang.org/x/xerrors"
)

func fmtErrorf(format string, args ...interface{}) error {
	return xerrors.Errorf(format, args...)
}

func errorAs(err error, target interface{}) bool {
	return xerrors.As(err, target)
}

func errorIs(err, target error) bool {
	return xerrors.Is(err, target)
}

func errorUnwrap(err error) error {
	return xerrors.Unwrap(err)
}
