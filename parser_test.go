/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// parser_test.go: Tests packet parser initialization

package pakparse_test

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func TestNonStructPacket(t *testing.T) {
	_, err := pakparse.New(reflect.TypeOf([]byte{}), nil)
	require.EqualError(t, err, pakparse.ErrExpectedStructType.Error())
}

func TestPrivateField(t *testing.T) {
	type Packet struct{ private int8 }
	p := mustCreateParser(t, Packet{}, nil)
	require.Equal(t, 0, p.NumField())
}

func TestOmittedField(t *testing.T) {
	type Packet struct {
		A uint16 `pakparse:"-"`
	}

	p := mustCreateParser(t, Packet{}, nil)
	require.Zero(t, p.NumField())
}

func TestParserInterface(t *testing.T) {
	type Packet struct{ V uint16 }
	p := mustCreateParser(t, Packet{}, nil)
	require.Equal(t, reflect.TypeOf(Packet{}), p.Type())
	require.Equal(t, 1, p.NumField())
	require.NotNil(t, p.Field(0))
}

func TestVerifyCheckFunction(t *testing.T) {
	type Packet struct{}
	var packet = reflect.TypeOf(Packet{})

	t.Run("InvalidType", func(t *testing.T) {
		_, err := pakparse.VerifyCheckFunction(reflect.TypeOf([]byte{}), nil)
		require.EqualError(t, err, pakparse.ErrExpectedStructType.Error())
	})

	t.Run("Bad", func(t *testing.T) {
		_, err := pakparse.VerifyCheckFunction(packet, func() {})
		require.EqualError(t, err, "check function must be a function accepting *pakparse_test.Packet and/or []byte and returning bool")
	})

	var good = map[string]interface{}{
		"Packet":      func(*Packet) bool { return true },
		"Bytes":       func([]byte) bool { return true },
		"PacketBytes": func(*Packet, []byte) bool { return true },
		"BytesPacket": func([]byte, *Packet) bool { return true },
	}

	for name, fn := range good {
		t.Run(name, func(t *testing.T) {
			_, err := pakparse.VerifyCheckFunction(packet, fn)
			require.NoError(t, err)
		})
	}
}

func TestVerifyLengthFunction(t *testing.T) {
	type Packet struct{}
	var packet = reflect.TypeOf(Packet{})
	var Err = "length function must be a function accepting *pakparse_test.Packet and returning an int type"

	t.Run("InvalidType", func(t *testing.T) {
		_, err := pakparse.VerifyLengthFunction(reflect.TypeOf([]byte{}), nil)
		require.EqualError(t, err, pakparse.ErrExpectedStructType.Error())
	})

	var bad = map[string]interface{}{
		"NotFunc": 1,
		"BadSig1": func() {},
		"BadSig2": func(*Packet) bool { return false },
	}
	for name, fn := range bad {
		t.Run(name, func(t *testing.T) {
			_, err := pakparse.VerifyLengthFunction(packet, fn)
			require.EqualError(t, err, Err)
		})
	}

	var good = map[string]interface{}{
		"Int":  func(*Packet) int { return 0 },
		"Uint": func(*Packet) uint { return 0 },
	}

	for name, fn := range good {
		t.Run(name, func(t *testing.T) {
			_, err := pakparse.VerifyLengthFunction(packet, fn)
			require.NoError(t, err)
		})
	}
}

func TestVerifyParserFunction(t *testing.T) {
	type Packet struct{}
	type State struct{}
	var packet = reflect.TypeOf(Packet{})
	var Err = "parser function must be a function with the signature func(bool, []byte, STATE, *pakparse_test.Packet) (int, STATE, Status)"

	t.Run("InvalidType", func(t *testing.T) {
		_, err := pakparse.VerifyParserFunction(reflect.TypeOf([]byte{}), nil)
		require.EqualError(t, err, pakparse.ErrExpectedStructType.Error())
	})

	var bad = map[string]interface{}{
		"NotFunc": 1,
		"BadSig1": func() {},
		"BadSig2": func(a, b, c, d int) (x, y, z int) { return 0, 0, 0 },
		"BadCovariant": func(bool, []byte, *State, *Packet) (uint, interface{}, pakparse.Status) {
			return 0, nil, pakparse.Invalid
		},
	}
	for name, fn := range bad {
		t.Run(name, func(t *testing.T) {
			_, err := pakparse.VerifyParserFunction(packet, fn)
			require.EqualError(t, err, Err)
		})
	}

	var good = map[string]interface{}{
		"Int":  func(bool, []byte, *State, *Packet) (int, *State, pakparse.Status) { return 0, nil, pakparse.Invalid },
		"Uint": func(bool, []byte, *State, *Packet) (uint, *State, pakparse.Status) { return 0, nil, pakparse.Invalid },
		"Covariant": func(bool, []byte, interface{}, *Packet) (uint, *State, pakparse.Status) {
			return 0, nil, pakparse.Invalid
		},
	}

	for name, fn := range good {
		t.Run(name, func(t *testing.T) {
			_, err := pakparse.VerifyParserFunction(packet, fn)
			require.NoError(t, err)
		})
	}
}

func TestStopSequenceAssignment(t *testing.T) {
	var tests = map[string]struct {
		Type  interface{}
		Error string
	}{
		"Double":  {struct{ V, U []byte }{}, "field V: expected fixed byte field (stop sequence), found another variable length field"},
		"Missing": {struct{ V []byte }{}, "field V: expected fixed byte field (stop sequence), reached end of structure"},
		"Proper": {struct {
			V []byte
			S uint8 `pakparse:"value=0xFF"`
		}{}, ""},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			_, err := pakparse.New(reflect.TypeOf(test.Type), nil)
			if test.Error == "" {
				require.NoError(t, err)
			} else {
				require.EqualError(t, err, test.Error)
			}
		})
	}
}
