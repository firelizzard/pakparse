/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// decoder.go: Core packet decoder implementation

package pakparse

import (
	"io"
	"reflect"
	"sync/atomic"
	"time"
)

// MaxZeroByteReadCount holds the number of Read() attempts in a row that return (n=0, err=nil) before failing
const MaxZeroByteReadCount = 100

// Custom errors that pakparse can return.  Other errors from other packages might also be returned.
var (
	ErrClosed        = io.EOF
	ErrReadZeroBytes = fmtErrorf("Read() returned 0 bytes %d times; bailing out", MaxZeroByteReadCount)
)

type decoderImpl interface{ decode(*Parser, *State) error }

const (
	stateCreated = -1
	stateRunning = 0
	stateStopped = 1
)

// Decoder decodes packets from an io.Reader
type Decoder struct {
	parser *Parser
	pstate *State
	impl   decoderImpl

	start func(func())
	state int32
}

func (d *Decoder) lazyStart() {
	if !atomic.CompareAndSwapInt32(&d.state, stateCreated, stateRunning) {
		return
	}

	if d.start == nil {
		return
	}

	ch := make(chan struct{})
	started := func() { close(ch) }
	d.start(started)
	<-ch
}

// NewDecoder creates a new decoder.  The parser is built using reflection to look at the fields/tags of the struct
// type in typ.
//
// The reader must be a ReadCloser or support deadlines, to prevent leaking goroutines.
func NewDecoder(parser *Parser, r io.Reader) *Decoder {
	var impl decoderImpl
	var start func(func())

	if rdl, ok := r.(interface{ SetReadDeadline(time.Time) error }); ok {
		impl = &stoppableDecoder{setDeadline: rdl.SetReadDeadline}

	} else if dl, ok := r.(interface{ SetDeadline(time.Time) error }); ok {
		impl = &stoppableDecoder{setDeadline: dl.SetDeadline}

	} else if _, ok := r.(io.Closer); ok {
		impl = &timeoutDecoder{}

	} else {
		impl = &simpleDecoder{source: r}
	}

	if s, ok := impl.(interface {
		start(r io.Reader, started func())
	}); ok {
		start = func(started func()) { go s.start(r, started) }
	}

	return &Decoder{parser, new(State), impl, start, stateCreated}
}

// NewDecoderWithChannel creates a new decoder that reads from a <-chan []byte
//
// SetPacketTimeout will succeed. Close will fail.
func NewDecoderWithChannel(parser *Parser, ch <-chan []byte) *Decoder {
	impl := &channelDecoder{source: ch}
	return &Decoder{parser, new(State), impl, nil, stateCreated}
}

// BytesConsumed calls State.BytesConsumed
func (d *Decoder) BytesConsumed() int { return d.pstate.BytesConsumed() }

// BytesDiscarded calls State.BytesDiscarded
func (d *Decoder) BytesDiscarded() int { return d.pstate.BytesDiscarded() }

// PacketsParsed calls State.PacketsParsed
func (d *Decoder) PacketsParsed() int { return d.pstate.PacketsParsed() }

// ResetStats calls State.ResetStats
func (d *Decoder) ResetStats() { d.pstate.ResetStats() }

// SetPacketTimeout sets a maximum length of time to wait for a byte to be
// received.  This allows a specified delay between bytes to drop the current
// working state and return to searching for the first struct field.
func (d *Decoder) SetPacketTimeout(t time.Duration) error {
	x, ok := d.impl.(interface{ setTimeout(t time.Duration) })
	if !ok {
		return fmtErrorf("cannot set timeout: not supported by reader")
	}

	x.setTimeout(t)
	return nil
}

// SetReadTimeout is used to set the (read) deadline of the underlying reader,
// if the reader supports setting deadlines.
//
// This timeout can be used to ensure that the runloop used for reader-based
// implementations blocks for a maximum duration, ensuring that the decoder can
// be properly closed.
func (d *Decoder) SetReadTimeout(t time.Duration) error {
	x, ok := d.impl.(interface{ setReadTimeout(t time.Duration) })
	if !ok {
		return fmtErrorf("cannot set read timeout: not supported by reader")
	}

	x.setReadTimeout(t)
	return nil
}

// Close terminates the parser's goroutines and cleans up
func (d *Decoder) Close() error {
	x, ok := d.impl.(interface{ stop() })
	if !ok {
		return fmtErrorf("decoder cannot be closed")
	}

again:
	switch atomic.LoadInt32(&d.state) {
	case stateCreated:
		if atomic.CompareAndSwapInt32(&d.state, stateCreated, stateStopped) {
			return nil
		}
		goto again

	case stateRunning:
		if atomic.CompareAndSwapInt32(&d.state, stateRunning, stateStopped) {
			x.stop()
			return nil
		}
		goto again

	default:
		return nil
	}
}

// Decode dequeues the oldest decoded packet. If no packets have been enqueued,
// Decode reads bytes from its data source until a packet has been decoded.
func (d *Decoder) Decode(v interface{}) error {
	u := reflect.ValueOf(v).Elem()
	if !u.CanSet() {
		return fmtErrorf("cannot set *v")
	}

	d.lazyStart()
	if atomic.LoadInt32(&d.state) != stateRunning {
		return ErrClosed
	}

	for {
		x, ok := d.pstate.Next()
		if ok {
			u.Set(reflect.ValueOf(x))
			return nil
		}

		err := d.impl.decode(d.parser, d.pstate)
		if err != nil {
			return err
		}
	}
}
