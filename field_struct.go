/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// field_struct.go: Core field parsing

package pakparse

import (
	"encoding/binary"
	"reflect"
)

type structFieldParser struct {
	name   string // name of field
	idx    int    // index of the field in the struct
	config *FieldConfig

	marshaller   Marshaller
	unmarshaller Unmarshaller
}

func (f *structFieldParser) Name() string { return f.name }

func (f *structFieldParser) Order() binary.ByteOrder {
	if f.config.Order != nil {
		return f.config.Order
	}
	return binary.BigEndian
}

func (f *structFieldParser) Reset(interface{}, reflect.Value) interface{} {
	return nil
}

func (f *structFieldParser) marshalValue(packet reflect.Value) []byte {
	v := reflect.Indirect(packet).Field(f.idx)
	if f.marshaller == nil {
		f.marshaller = NewValueMarshaller(v.Type(), f.Order(), f.config.Width.Calculate(packet))
	}

	return f.marshaller(v)
}

func (f *structFieldParser) unmarshalValue(data []byte, packet reflect.Value) {
	v := reflect.Indirect(packet).Field(f.idx)
	if f.unmarshaller == nil {
		f.unmarshaller = NewValueUnmarshaller(v.Type(), f.Order(), f.config.Width.Calculate(packet))
	}

	f.unmarshaller(data, v)
}
